# Send times from your commit-messages to a [Tine2.0](https://tine20.org) instance

A small addition to [CaptainHook](https://packagist.org/captainhook/captainhook) that 
will send timing-entries from your commit-messages to your Tine2.0 instance.

That way you can track your times via your commit-messages.

This is a backend implementation for the [sendtime](https://packagist.org/org_heigl/captainhook_sendtime)-addon 
to CaptainHook

## Usage

Add this to your `captainhook.json`-File. Make sure to adapt the relevant values.

```json
{
  "post-commit" : {
    "enabled" : true,
    "actions" : [{
      "action": "\\Org_Heigl\\CaptainHook\\Hooks\\SendTime\\SendTimeAction",
      "options": {
        "account" : "Tine2.0 Action-ID",
        "backendfactory" : "\\Org_Heigl\\CaptainHook\\Hooks\\SendTimeTine2Backend\\Tine2Factory",
        "username" : "Tine 2.0 Username",
        "passwordfile" : "Path/to/a/file/containing/your/Tine2.0/login/password",
        "uri" : "https://tine20.example.net"
      }
    }]
  }
 }
```