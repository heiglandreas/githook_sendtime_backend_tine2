<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTimeTine2Backend;


use CaptainHook\App\Config\Options;
use function file_get_contents;
use Graze\GuzzleHttp\JsonRpc\Client;
use GuzzleHttp\Cookie\CookieJar;
use Org_Heigl\CaptainHook\Hooks\SendTime\Backend\Backend;
use Org_Heigl\CaptainHook\Hooks\SendTime\Backend\BackendFactory;
use UnexpectedValueException;

class Tine2Factory implements BackendFactory
{
    public function createBackend(Options $options) : Backend
    {
        $uri = $options->get('uri');
        $username = $options->get('username');
        $password = trim(file_get_contents($options->get('passwordfile')));

        $jar = new CookieJar();

        $client = Client::factory($uri, [
            'cookies' => $jar,
        ]);

        $header = [];

        $response = $client->send($client->request(123, 'Tinebase.login', [
            'username' => $username,
            'password' => $password,
        ]));

        $result = $response->getRpcResult();

        if ($result['success'] !== true) {
            throw new UnexpectedValueException('Login did not work');
        }

        $header['X-Tine20-JsonKey'] = $result['jsonKey'];
        $client = Client::factory($uri, [
            'headers' => $header,
            'cookies' => $jar,
        ]);

        return new Tine2($client, $result['account']['accountId']);
    }
}