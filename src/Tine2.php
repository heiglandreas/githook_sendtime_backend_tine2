<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\SendTimeTine2Backend;


use DateInterval;
use Exception;
use Graze\GuzzleHttp\JsonRpc\Client;
use Org_Heigl\CaptainHook\Hooks\SendTime\Backend\Backend;
use Org_Heigl\CaptainHook\Hooks\SendTime\Timesheet;

class Tine2 implements Backend
{
    private $client;

    private $accountId;

    public function __construct(Client $client, string $accountId)
    {
        $this->client = $client;
        $this->accountId = $accountId;
    }

    public function sendTimesheet(Timesheet $timesheet) : void
    {
        $request = $this->client->request(123, 'Timetracker.saveTimesheet', [
            'recordData' => [
                'is_billable' => true,
                'is_cleared' => false,
                'start_date' => $timesheet->getDate()->format('Y-m-d H:i:s'),
                'duration' => (string) $this->getDurationInMinutes($timesheet->getDuration()),
                'seq' => 0,
                'is_deleted' => 0,
                'account_id' => $this->accountId,
                'account_grants' => [
                    'addGrant' => true,
                    'editGrant' => true,
                ],
                'timeaccount_id' => $timesheet->getAccount(),
                'billed_in' => '',
                'start_time' => '',
                'description' => $timesheet->getDescription(),
                'tags' => [],
                'customfields' => [],
                'relations' => [],
                'attachments' => [],
                'notes' => []
            ],
            'context' => [
                'skipClosedCheck' => false,
            ]
        ]);

        $this->client->send($request);
    }

    public function __destruct()
    {
        try {
            $this->client->send($this->client->request(123, 'Tinebase.logout', []));
        } catch (Exception $e) {
            //
        }
    }

    private function getDurationInMinutes(DateInterval $duration) : int
    {
        // Conversion to minutes is done with these default values: 60 minutes in an Hour, 24 Hours in a day,
        // 30 days in a month, 12 months in a year. Seconds are ignored.
        $minutes = 0;

        if ($duration->days > 0) {
            $minutes = $minutes + 60 * 24 * $duration->days;
        } else {
            if ($duration->y > 0) {
                $minutes = $minutes + 60 * 24 * 30 * 12 * $duration->y;
            }

            if ($duration->m > 0) {
                $minutes = $minutes + 60 * 24 * 30 * $duration->m;
            }

            if ($duration->d > 0) {
                $minutes = $minutes + 60 * 24 * $duration->m;
            }
        }

        if ($duration->h > 0) {
            $minutes = $minutes + 60 * $duration->h;
        }

        if ($duration->i > 0) {
            $minutes = $minutes + $duration->i;
        }

        return $minutes;
    }
}